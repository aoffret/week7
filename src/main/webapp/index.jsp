<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
<h1><%= "Please enter the following book information." %>
</h1>
    <form action="servlet" method="post">
        <p>Book Name<input name="bookName" type="text"></p>
        <p>Author<input name="author" type = "text"></p>
        <p>Last Checked Out<input name="lastCheckedOut" type = "text"></p>
        <p><input type="submit" value="Input Information"></p>
    </form>

<br/>
<a href="/servlet">Click Here</a>
</body>
</html>