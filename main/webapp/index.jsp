<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
<h1><%= "Please enter the following book information." %>
</h1>
    <form action="servlet" method="post">
        <p>Book Name: <input name="bookName" type="text"></p>
        <p>Author: <input name="author" type = "text"></p>
        <p>ISBN (Numbers must be 13 digits long): <input name="isbn" type = "text"></p>
        <p><input type="submit" value="Add the Book"></p>
    </form>

<br/>
<a href="servlet">See the list of books</a>
</body>
</html>