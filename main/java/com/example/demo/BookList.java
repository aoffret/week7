package com.example.demo;

import java.io.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.util.*;
//ideas to put in limit of length for each length. If there is an error, send back the error and the chance to try again.

@WebServlet(name = "servlet",urlPatterns={"/servlet"})
public class BookList extends HttpServlet {

    //make a funciton that helps collection to sort (ex: name of a book)
    //put back to treeset later
    public static ArrayList<ObjectBookTemplate> listOfBooks = new ArrayList<>();
    public int numOfObjects = 0;


    //Uses to redirect you, won't work without this
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String bookName = request.getParameter("bookName");
        String author = request.getParameter("author");
        String isbnString = request.getParameter("isbn");
        long isbn;

        try {
            isbn = Long.parseLong(isbnString);
            long num = isbn;
            int digits = 0;
            //calculate num of digit
            while (num != 0){
                num /= 10;
                digits++;
                out.println(digits + " ");
            }
            if (digits != 13){
                //out.println("Invalid Input. Please hit the back arrow and put in a valid ISBN.");
                throw new NumberFormatException("Invalid Input. Please hit the back arrow and put in a valid ISBN.");
            }

            //create object and input the information
            ObjectBookTemplate obj = new ObjectBookTemplate();
            obj.setName(bookName);
            obj.setAuthor(author);
            obj.setIsbn(isbn); // If the string does not contain a valid integer then it will throw a NumberFormatException.

            // Prehaps put limits and errors here. (ex: how long does the ISBN need to be, what happens if the user don't meet it?)

            listOfBooks.add(obj);
            numOfObjects++;

           out.println("The book: " + obj.name + " was processed. Please hit the back arrow to enter another book.");

        } catch (NumberFormatException e){
            out.println("Invalid Input. Please hit the back arrow and put in whole numbers into the ISBN.");
        }



        //}
        //show you added
        //put this on another page or put it into the doGet section

    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        out.println("<html><body>");
        //out.println(listOfBooks);
        out.println();
        if (listOfBooks.get(0) == null){
            out.println("There are no books listed. Please hit the back arrow and hit a book.");
        }
        else {
            for (int i = 0; i < numOfObjects; i++){
                out.println( "Book " + (i+1) + " Name: " + listOfBooks.get(i).name + " Author: " + listOfBooks.get(i).author + " ISBN: " + (char)listOfBooks.get(i).isbn + "<br/>");
            }
        }
        //find way to print string info
        out.println("</body></html>");

    }


    public void destroy() {
    }


}
/*
other notes: When running tomcat, will auto setup war and execute html code
Need index file
If have hard problem and can't submit --> mabye something wrong with the @WebServlet line
If nothing works, make sure see no errors in output
 */