package com.example.demo;

public class ObjectBookTemplate {


    //variables that user will put in
    public String name;
    public String author;
    public long isbn;
    public String category;

    //functions that set the values
    public void setName(String name) {
        this.name = name;
    }
    public void setAuthor(String author) {
        this.author = author;
    }

    public void setIsbn(long isbn){
        this.isbn = isbn;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}

