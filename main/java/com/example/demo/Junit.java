package com.example.demo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Junit {
    private ObjectBookTemplate obj = new ObjectBookTemplate();
    public ObjectBookTemplate obj1 = new ObjectBookTemplate();

    @Before
    public void setUp() {
        //set up values
        obj.setName("GeneralTestName");
        obj.setAuthor("GeneralTestAuthor");
        String num = "1111122222333";
        long isbn = Long.parseLong(num);
        obj.setIsbn(isbn);
    }

    @Test
    public void testObject() {
        ObjectBookTemplate obj1 = new ObjectBookTemplate();
        obj1.setName("NameTest");
        assertEquals(obj1.name, "NameTest");

        obj1.setAuthor("AuthorTest");
        assertEquals(obj1.author, "AuthorTest");

        //sets up isbn
        String num = "1111122222333";
        long isbn = Long.parseLong(num);
        obj1.setIsbn(isbn);
        assertEquals(obj1.isbn, isbn);
    }

    //test if the array list is working right
    @Test
    public void testArrayList() {
        BookList.listOfBooks.add(obj);
        BookList.listOfBooks.add(obj1);
        Assert.assertSame(obj, BookList.listOfBooks.get(0));
        Assert.assertSame(obj1, BookList.listOfBooks.get(1));
    }
}
